function changeStatus(el){
    if(el.style.backgroundColor === "grey"){
        el.style.backgroundColor = "green";
    }else{
        el.style.backgroundColor = "grey";
    }
}

document.getElementById("img-new-messages").addEventListener("dblclick", function(){
    let ind = document.getElementById("indicator");
    if(ind.style.display == "none"){
        ind.style.display = "block";
    }else{
        ind.style.display = "none";
    }
});

let IsModeForAdd;
let curRow;
document.querySelectorAll("button [alt='Delete']")
    .forEach(el => el.addEventListener("click", function deleteRowInTable(){
        document.getElementById("modal-delete-student").style.display = "flex";
        document.getElementById("modal-window-container").style.display = "flex";
        curRow = el.closest("tr");
}));
document.querySelectorAll("button [alt='Edit']")
    .forEach(el => el.addEventListener("click", function editRowInTable(){
        document.getElementById("modal-add-student").style.display = "flex";
        document.getElementById("modal-window-container").style.display = "flex";
        document.getElementsByClassName("header-title")[1].innerHTML = "Edit student";
        curRow = el.closest("tr");
        IsModeForAdd = false;

        let form = document.forms["student-form"];
        /* fill the form with current row fields */
        form["group"].value = curRow.children[1].innerHTML;
        form["first-name"].value = firstWord(curRow.children[2].innerHTML);
        form["last-name"].value = lastWord(curRow.children[2].innerHTML);
        form["gender"].value = curRow.children[3].innerHTML;
        form["date"].value = curRow.children[4].innerHTML[6] + curRow.children[4].innerHTML[7] +
            curRow.children[4].innerHTML[8] + curRow.children[4].innerHTML[9] + "-" +
            curRow.children[4].innerHTML[3] + curRow.children[4].innerHTML[4] + "-" +
            curRow.children[4].innerHTML[0] + curRow.children[4].innerHTML[1];
}));
let modalDeleteStudent = document.getElementById("modal-delete-student");
modalDeleteStudent.querySelector("button.modal-answer-button[value='OK']").addEventListener("click", function(){
    curRow.parentElement.removeChild(curRow);
    document.getElementById("modal-delete-student").style.display = "none";
    document.getElementById("modal-window-container").style.display = "none";
});
modalDeleteStudent.querySelector("button.modal-answer-button[value='Cancel']").addEventListener("click", function(){
    document.getElementById("modal-delete-student").style.display = "none";
    document.getElementById("modal-window-container").style.display = "none";
});
modalDeleteStudent.querySelector("button.but").addEventListener("click", function(){
    document.getElementById("modal-delete-student").style.display = "none";
    document.getElementById("modal-window-container").style.display = "none";
});

document.querySelector("button.but").addEventListener("click", function(){
    document.getElementById("modal-add-student").style.display = "flex";
    document.getElementById("modal-window-container").style.display = "flex";
    document.getElementsByClassName("header-title")[1].innerHTML = "Add new student";
    IsModeForAdd = true;
});
let modalAddStudent = document.getElementById("modal-add-student");
modalAddStudent.querySelector("button.modal-answer-button[value='Confirm']").addEventListener("click", AddOrEditStudent);
modalAddStudent.querySelector("button.modal-answer-button[value='Confirm']").addEventListener("click", AddOrEditStudent);
modalAddStudent.querySelector("button.modal-answer-button[value='Cancel']").addEventListener("click", function(){
    document.getElementById("modal-add-student").style.display = "none";
    document.getElementById("modal-window-container").style.display = "none";
});
modalAddStudent.querySelector("button.but").addEventListener("click", function(){
    document.getElementById("modal-add-student").style.display = "none";
    document.getElementById("modal-window-container").style.display = "none";
});

function AddOrEditStudent(){
    let form = document.forms["student-form"];
    if(IsModeForAdd){
        if(form["group"].value != "" && form["first-name"].value!= "" 
            && form["last-name"].value!="" && form["date"].value != ""
            && form["gender"].value != "")
        {
            let newRow = document.createElement("tr");

            let td1 = document.createElement("td");
            let checkBox1 = document.createElement("input");
            checkBox1.type = "checkbox";
            td1.appendChild(checkBox1);
            newRow.appendChild(td1);

            let td2 = document.createElement("td");
            td2.innerHTML = form["group"].value;
            newRow.appendChild(td2);

            let td3 = document.createElement("td");
            td3.innerHTML = form["first-name"].value + " " + form["last-name"].value;
            newRow.appendChild(td3);

            let td4 = document.createElement("td");
            td4.innerHTML = form["gender"].value;
            newRow.appendChild(td4);

            let td5 = document.createElement("td");
            let date = new Date(form["date"].value);
            let yyyy = date.getFullYear();
            let dd = date.getDate();
            let mm = date.getMonth() + 1;

            if (dd < 10) dd = '0' + dd;
            if (mm < 10) mm = '0' + mm;

            let textDate = dd + '.' + mm + '.' + yyyy;
            td5.innerHTML =  textDate;
            newRow.appendChild(td5);

            let td6 = document.createElement("td");
            let statusDiv =  document.createElement("div");
            statusDiv.className = "status-circle-in-table";
            statusDiv.setAttribute("onclick", "changeStatus(this)");
            td6.appendChild(statusDiv);
            newRow.appendChild(td6);

            let td7 = document.createElement("td");
            let butEdit = document.createElement("button");
            butEdit.className = "but";
            let imgEdit = document.createElement("img");
            imgEdit.src = "pencil.svg";
            imgEdit.alt = "Edit";
            imgEdit.className = "img-in-but";
            butEdit.appendChild(imgEdit);
            butEdit.addEventListener("click", function editRowInTable(){
                $("#modal-add-student").css("display", "flex");
                $("#modal-window-container").css("display", "flex");
                $(".header-title", 0).html("Edit student");
                curRow = butEdit.closest("tr");
                IsModeForAdd = false;

                let form = document.forms["student-form"];
                /* fill the form with current row fields */
                form["group"].value = curRow.children[1].innerHTML;
                form["first-name"].value = firstWord(curRow.children[2].innerHTML);
                form["last-name"].value = lastWord(curRow.children[2].innerHTML);
                form["gender"].value = curRow.children[3].innerHTML;
                form["date"].value = curRow.children[4].innerHTML[6] + curRow.children[4].innerHTML[7] +
                    curRow.children[4].innerHTML[8] + curRow.children[4].innerHTML[9] + "-" +
                    curRow.children[4].innerHTML[3] + curRow.children[4].innerHTML[4] + "-" +
                    curRow.children[4].innerHTML[0] + curRow.children[4].innerHTML[1];
            });
            td7.appendChild(butEdit);
            let butDel = document.createElement("button");
            butDel.className = "but";
            let imgDel = document.createElement("img");
            imgDel.src = "close.jpg";
            imgDel.alt = "Delete";
            imgDel.className = "img-in-but";
            butDel.appendChild(imgDel);
            butDel.addEventListener("click", function deleteRowInTable(){
                document.getElementById("modal-delete-student").style.display = "flex";
                curRow = butDel.closest("tr");
            });
            td7.appendChild(butDel);
            newRow.appendChild(td7);

            $("tbody")[0].appendChild(newRow);

            $("#modal-add-student").css("display", "none");
            $("#modal-window-container").css("display", "none");
            clearFormFields();

            // new item for server
            let newStudentDataForServer = newRow.children[1].innerHTML + " "
                                        + newRow.children[2].innerHTML + " "
                                        + newRow.children[3].innerHTML + " "
                                        + newRow.children[4].innerHTML;
            // doing something to send data
        }else{
            alert("Fill all fields!!!");
        }
    }else{
        if(form["group"].value != "" && form["first-name"].value!= "" && form["last-name"].value!="" 
        && form["date"].value != "" && form["gender"].value != "")
        {
            curRow.children[1].innerHTML = form["group"].value;
            curRow.children[2].innerHTML = form["first-name"].value + " " + form["last-name"].value;
            curRow.children[3].innerHTML = form["gender"].value;

            let date = new Date(form["date"].value);
            let yyyy = date.getFullYear();
            let dd = date.getDate();
            let mm = date.getMonth() + 1;

            if (dd < 10) dd = '0' + dd;
            if (mm < 10) mm = '0' + mm;

            let textDate = dd + '.' + mm + '.' + yyyy;
            curRow.children[4].innerHTML =  textDate;

            $("#modal-add-student").css("display", "none");
            $("#modal-window-container").css("display", "none");
            clearFormFields();
        }else{
            alert("Fill all fields!!!");
        }
    }
}

function clearFormFields(){
    let form = document.forms["student-form"];
    form["group"].value = "";
    form["first-name"].value = "";
    form["last-name"].value = "";
    form["gender"].value = "";
    form["date"].value = "";
}

function firstWord(str) {
    let result = '';
    for (let i = 0; i < str.length; ++i) {
      if (str.charAt(i) === ' ')
        break;
      else
        result += str.charAt(i);
    }
    return result;
}
function lastWord(str) {
    let i = 0;
    while(str.charAt(i) !== " ") ++i;
    ++i; 
    let result = '';
    for (; i < str.length; ++i) {
        result += str.charAt(i);
    }
    return result;
}

window.addEventListener('load', async () => {
    if ('serviceWorker' in navigator) {
      try {
        const reg = await navigator.serviceWorker.register('/sw.js')
        console.log('Service worker register success', reg)
      } catch (e) {
        console.log('Service worker register fail')
      }
    }
})