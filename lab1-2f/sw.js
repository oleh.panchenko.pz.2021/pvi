const staticCacheName = 's-app-v3'

const assetUrls = [
  'students.html',
  'scrypt.js',
  'style.css'
]

self.addEventListener('install', async event => {
  const cache = await caches.open(staticCacheName)
  await cache.addAll(assetUrls)
})