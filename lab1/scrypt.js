function changeStatus(el){
    if(el.style.backgroundColor === "grey"){
        el.style.backgroundColor = "green";
    }else{
        el.style.backgroundColor = "grey";
    }
}

document.getElementById("img-new-messages").addEventListener("dblclick", function(){
    let ind = document.getElementById("indicator");
    if(ind.style.display == "none"){
        ind.style.display = "block";
    }else{
        ind.style.display = "none";
    }
});


let curRow;
let buttonsDeleteRow = document.querySelectorAll("button [alt='Delete']")
    .forEach(el => el.addEventListener("click", function deleteRowInTable(){
        document.getElementById("modal-delete-student").style.display = "flex";
        curRow = el.closest("tr");
}));
let modalDeleteStudent = document.getElementById("modal-delete-student");
modalDeleteStudent.querySelector("button.modal-answer-button[value='OK']").addEventListener("click", function(){
    curRow.parentElement.removeChild(curRow);
    document.getElementById("modal-delete-student").style.display = "none";
});
modalDeleteStudent.querySelector("button.modal-answer-button[value='Cancel']").addEventListener("click", function(){
    document.getElementById("modal-delete-student").style.display = "none";
});
modalDeleteStudent.querySelector("button.but").addEventListener("click", function(){
    document.getElementById("modal-delete-student").style.display = "none";
});

document.querySelector("button.but").addEventListener("click", function(){
    document.getElementById("modal-add-student").style.display = "flex";
});
let modalAddStudent = document.getElementById("modal-add-student");
modalAddStudent.querySelector("button.modal-answer-button[value='Add']").addEventListener("click", function(){
    let newRow = document.createElement("tr");

    let td1 = document.createElement("td");
    let checkBox1 = document.createElement("input");
    checkBox1.type = "checkbox";
    td1.appendChild(checkBox1);
    newRow.appendChild(td1);

    let td2 = document.createElement("td");
    td2.innerHTML = "KN-24";
    newRow.appendChild(td2);

    let td3 = document.createElement("td");
    td3.innerHTML = "Garry Lastwood";
    newRow.appendChild(td3);

    let td4 = document.createElement("td");
    td4.innerHTML = "M";
    newRow.appendChild(td4);

    let td5 = document.createElement("td");
    td5.innerHTML = "21.12.2012";
    newRow.appendChild(td5);

    let td6 = document.createElement("td");
    let statusDiv =  document.createElement("div");
    statusDiv.className = "status-circle-in-table";
    statusDiv.setAttribute("onclick", "changeStatus(this)");
    td6.appendChild(statusDiv);
    newRow.appendChild(td6);

    let td7 = document.createElement("td");
    let butEdit = document.createElement("button");
    butEdit.className = "but";
    let imgEdit = document.createElement("img");
    imgEdit.src = "pencil.svg";
    imgEdit.alt = "Edit";
    imgEdit.className = "img-in-but";
    butEdit.appendChild(imgEdit);
    td7.appendChild(butEdit);
    let butDel = document.createElement("button");
    butDel.className = "but";
    let imgDel = document.createElement("img");
    imgDel.src = "close.jpg";
    imgDel.alt = "Delete";
    imgDel.className = "img-in-but";
    butDel.appendChild(imgDel);
    butDel.addEventListener("click", function deleteRowInTable(){
        document.getElementById("modal-delete-student").style.display = "flex";
        curRow = butDel.closest("tr");
    });
    td7.appendChild(butDel);
    newRow.appendChild(td7);

    document.getElementById("table1").getElementsByTagName("tbody")[0].appendChild(newRow);

    document.getElementById("modal-add-student").style.display = "none";
});
modalAddStudent.querySelector("button.modal-answer-button[value='Cancel']").addEventListener("click", function(){
    document.getElementById("modal-add-student").style.display = "none";
});
modalAddStudent.querySelector("button.but").addEventListener("click", function(){
    document.getElementById("modal-add-student").style.display = "none";
});